# IMAGE SERVER #

This package allows the processing of images based on request information read from the HTTP Header or cookies set by the browser. Using the meta information the image url is rewritten so the image returned is altered to fit best for the requesting party. E.g. if the request comes from a device with a small screen it returns a lowres image. Since this cannot be derived from the user-agent without including tablets it checks a cookie value called ImageQuality, which needs to be set by the requester (in the browser). By setting the cookies to the beginning of the page reques all subsequent requests to images will also contain the cookie information.

### What it installs ###
* Adds UrlRewriter rules in the web.config
* Adds a HTTP module declaration to the web.config to run security checks and process DPI rewrites
* Adds the PreProcessing module to rewrite width and height parameters in the original requests and to run security checks
* Adds 4 settings to your web.config to set security stuff

### How to use it ###
* Install this package in a project using ImageProcessor or which does not have an image processing solution yet. For better performance use a separate project that only processes images and runs no a subdomain images.mywebsite.com
* Add the following JavaScript lines at the beginning of the head:

*hosted on the same domain*
```
     <script>
        if (screen.width <= 480) {
            document.cookie = 'imagequality=30; path=/; domain=' + window.location.host;
        }
        else {
            document.cookie = 'imagequality=90; path=/; domain=' + window.location.host;
        }
        
        //UNCOMMENT FOLLOWING IF YOU WANT TO SUPPORT HIGH RES IMAGES FOR RETINA SCREENS
        //if (screen.width > 480) {
        //    document.cookie = 'imagedpi=' + window.devicePixelRatio + '; path=/; domain=' + window.location.host;
        //}
        //else {
        //    document.cookie = 'imagedpi=1; path=/; domain=' + window.location.host;
        //}
    </script>
```

*hosted on a separate domain*
```
 <script>
        if (screen.width <= 480) {
            document.cookie = 'imagequality=30; path=/; domain=' + window.location.host.substring(window.location.host.indexOf('.') + 1);
        }
        else {
            document.cookie = 'imagequality=90; path=/; domain=' + window.location.host.substring(window.location.host.indexOf('.') + 1);
        }

        //UNCOMMENT FOLLOWING IF YOU WANT TO SUPPORT HIGH RES IMAGES FOR RETINA SCREENS
        //if (screen.width > 480) {
        //    document.cookie = 'imagedpi=' + window.devicePixelRatio + '; path=/; domain=' + window.location.host.substring(window.location.host.indexOf('.') + 1);
        //}
        //else {
        //    document.cookie = 'imagedpi=1; path=/; domain=' + window.location.host.substring(window.location.host.indexOf('.') + 1);
        //}
    </script>
```

### Security ###
A common problem with imageprocessors is that they can be abused to request random sizes, which results in high unwanted CPU usage processing the images. To avoid this ImageServer has the following protection build in:

* Image requests are only accepted from predefined referrer hosts
* Direct image requests (empty or unknown referrer) are only accepted from predefined IP's
* If non of the above match the request is rewritten to a predefined querystring, eg. width=1024
* The above is only active for non-DEBUG environments.