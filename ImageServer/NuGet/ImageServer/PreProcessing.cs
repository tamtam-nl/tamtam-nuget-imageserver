﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using System.Linq;
using System.Collections.Specialized;
using System.Configuration;

namespace TamTam.NuGet.ImageServer
{
    public class PreProcessing : IHttpModule
    {
        HttpApplication _application = null;
        private static string[] _whitelistIPs = (ConfigurationManager.AppSettings["TT.ImageServer.Security.WhitelistIPs"] ?? string.Empty).Split(',');
        private static string[] _whitelistRefs = (ConfigurationManager.AppSettings["TT.ImageServer.Security.WhitelistReferrers"] ?? string.Empty).Split(',');
        private static string _querystring = (ConfigurationManager.AppSettings["TT.ImageServer.Security.DefaultQuerystring"] ?? string.Empty);
        private static string[] _extensions = (ConfigurationManager.AppSettings["TT.ImageServer.Security.Extensions"] ?? string.Empty).Split(',');

        public void Init(HttpApplication context)
        {
            context.BeginRequest += context_BeginRequest;
            _application = context;
        }

        private void context_BeginRequest(object sender, System.EventArgs e)
        {
            //Quit if not correct extenstion
            var uri = _application.Context.Request.Url;
            var extension = uri.AbsolutePath.Split('.').Last().ToLower();
            if (!_extensions.Any(ext => ext == extension)) { return; }

            //Run the rewrite
            Rewrite();
        }

        private void Rewrite()
        {

            var uri = _application.Context.Request.Url;
            var qs = new NameValueCollection(_application.Context.Request.QueryString);

#if !DEBUG
            //Check referrer. If not existing referrer: rewrite to medium size (remove querystring) and quit
            var referrer = _application.Request.UrlReferrer;
            var clientIP = _application.Request.UserHostAddress;
            if (referrer == null || !_whitelistRefs.Any(host => host == referrer.Host))
            {
                if (clientIP != null && !_whitelistIPs.Any(ip => ip == clientIP))
                {
                    _application.Context.RewritePath(HttpUtility.UrlDecode(uri.AbsolutePath), null, _querystring);
                    return;
                }
            }
#endif

            //Run DPI check
            var dpi = 1d;
            var dpiVal = qs["dpi"] ?? "1";
            dpi = double.Parse(dpiVal, System.Globalization.CultureInfo.InvariantCulture);
            var rewrite = false;
            if (dpi > 1)
            {
                var widthVal = qs["width"] ?? "0";
                if (widthVal != "0")
                {
                    var width = int.Parse(widthVal);
                    qs["width"] = (width * dpi).ToString();
                    rewrite = true;
                }
                var heightVal = qs["height"] ?? "0";
                if (heightVal != "0")
                {
                    var height = int.Parse(heightVal);
                    qs["height"] = (height * dpi).ToString();
                    rewrite = true;
                }
            }

            //Rewrite DPI
            if (rewrite)
            {
                var query = qs.AllKeys.Select(k => string.Format("{0}={1}", k, qs[k]));
                var queryString = string.Join("&", query.ToArray());
                _application.Context.RewritePath(HttpUtility.UrlDecode(uri.AbsolutePath), null, queryString);
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}